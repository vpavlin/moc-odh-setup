# Running ODH on MOC using CRC

This repository should help you to deploy Open Data hub (ODH) in a VM on Mass Open Cloud (MOC) using Code Ready Containers (CRC). It assumes you already have access to MOC and can deploy a VM in their Open Stack instance.

1. Deploy a virtual machine instance. Make sure it has enough RAM (32 GB and more) and storage (50 GB and more) and it has access to internet (you need to create a new network and bridge it with `external` network in the Kaizen Open Stack UI). I used `Fedora 29` image for deployment.
2. Once the VM is deployed, attach a floating IP to it, so that you can SSH in and also access it from browser for Open Shift Console later
3. Get `Pull Secret` file from https://cloud.redhat.com/openshift/install/crc/installer-provisioned
4. Upload the `setup-moc.sh` and `pull-secret.txt` to the VM (following command assume you stored the pull secret in the cloned repo directory)

```
scp ./* fedora@${VM_IP}:/home/fedora/pull-secret.txt
```

5. SSH into the VM and run the setup script. As CRC is moving quite a lot of data around (VM images), the setup takes quite a lot of time

```
ssh fedora@${VM_IP}
```

If you want to enable monitoring on the cluster (i.e. OpenShift provided Prometheus and Grafana stack) set environment variable `CRC_MONITORING=1`. Keep in mind that this will make the infrastructure more CPU hungry!

```
bash setup-moc.sh
```

The setup script is based on [Nested virtualization in KVM](https://stafwag.github.io/blog/blog/2018/06/04/nested-virtualization-in-kvm/) and a Google Doc from a colleague descibing the Tinyproxy setup.

It first updates the packages (I have noticed some issues with older version os `seccomp`). Then it verifies the nested virtualisation works. It downloads Code Ready Containers and moves the binary on the `$PATH` location. 

It then runs CRC setup and start commands. You can obviously change the `crc start` arguments if you need. 

Lastly it configures Tinyproxy to enable port-forwarding from the OpenStack VM to a CRC VM so that you can access the OpenShift endpoints remotely. 

It also prints the `kubeadmin` password for OpenShift at the end.

By default the available users are `kubeadmin` and `developer`. As we use [AgnosticD](https://github.com/redhat-cop/agnosticd) (which generates user names in format `user1` to `userN`) to prepare our ODH environments for workshops like to have a user `user1` available. 

You can see files with `htpasswd` in name in the repo as well. These are also applied to OpenShift and create a `user1` user with password `openshift` which can then be used as an "ordinary" (developer) user in your OpenShift instance.

To access the OpenShift Console you need to configure an HTTP proxy in your browser, where the proxy URL/IP is the floating IP of your Open Stack VM (above mentioned as `${VM_IP}`) and port `8888`.

You should be able to access console at https://console-openshift-console.apps-crc.testing then.

To use commandline tools like `oc` or `curl`, you will need to set `https_proxy` environment variable

```
export https_proxy=http://${VM_IP}:8888
```

You should be able to login to OpenShift API with

```
oc login -u kubeadmin -p ${KUBEADMIN_PASSWORD} https://api.crc.testing:6443
```