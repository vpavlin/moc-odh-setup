#!/usr/bin/bash

sudo dnf -y update

if [ $(cat /sys/module/kvm_intel/parameters/nested) == "N" ]; then
    sudo modprobe -r kvm_intel
    sudo modprobe kvm_intel nested=1
    sudo bash -c 'echo "options kvm_intel nested=1" > /etc/modprobe.d/kvm_intel.conf'
    cat /sys/module/kvm_intel/parameters/nested
fi

crc &> /dev/null
if [ $? -gt 0 ]; then
    curl -O https://mirror.openshift.com/pub/openshift-v4/clients/crc/latest/crc-linux-amd64.tar.xz
    tar xf crc-linux-amd64.tar.xz
    mkdir -p ~/bin
    cp crc-linux-1.0.0-rc.0-amd64/crc ~/bin/
fi

crc setup
crc start --pull-secret-file pull-secret.txt --memory 32000 --cpus 8

sudo dnf -y install tinyproxy
sudo sed -i 's/Allow 127.0.0.1/#Allow 127.0.0.1/' /etc/tinyproxy/tinyproxy.conf
sudo sed -i 's/\(ConnectPort 563\)/ConnectPort 6443\nConnectPort 80\n/' /etc/tinyproxy/tinyproxy.conf

sudo systemctl start tinyproxy

crc console --credentials

curl -O https://mirror.openshift.com/pub/openshift-v4/clients/oc/4.2/linux/oc.tar.gz
tar xzf oc.tar.gz
cp oc ~/bin/

oc login -u kubeadmin -p `cat $(find ~/.crc/cache/ -iname kubeadmin-password)` --insecure-skip-tls-verify https://api.crc.testing:6443/

oc create secret generic htpasswd-secret -n openshift-config --from-file=htpasswd=./users.htpasswd
oc apply -f ./oauth-htpasswd.yaml

if [ -n "${CRC_MONITORING}" ]; then
    for service in `echo "prometheus-operator grafana cluster-monitoring-operator kube-state-metrics openshift-state-metrics prometheus-adapter"`; do
        echo $service
        oc -n openshift-monitoring scale deployment/${service} --replicas=1
    done
fi